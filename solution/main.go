package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/urfave/cli"
)

func fibonacci(n int, ch chan int, chQuit chan interface{}) {
	x, y := 0, 1
	for i := 0; i < n; i++ {
		select {
		case ch <- x:
			x, y = y, x+y
		case <-chQuit:
		}
	}
	close(ch)
}

func main() {
	var (
		count   int
		timeout int
	)

	app := cli.NewApp()
	app.Name = "fibonacci"
	app.Usage = "Generate Fibonacci numbers."
	app.Flags = []cli.Flag{
		cli.IntFlag{
			Name:        "count",
			Value:       5,
			Usage:       "Number of Fibonacci numbers to generate.",
			Destination: &count,
		},
		cli.IntFlag{
			Name:        "timeout",
			Value:       10,
			Usage:       "Timeout in nanoseconds.",
			Destination: &timeout,
		},
	}
	app.Action = func(ctx *cli.Context) error {
		ch := make(chan int, 10)
		chQuit := make(chan interface{}, 1)
		go fibonacci(count, ch, chQuit)
		go func() {
			for i := range ch {
				fmt.Println(i)
			}
		}()
		time.Sleep(time.Duration(timeout) * time.Nanosecond)
		chQuit <- struct{}{}
		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
