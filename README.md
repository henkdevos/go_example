# go_example

Example GO program with an exercise to add functionality, as part of my GO presentation on prezi.
Created for the Nalys Linux Cell.

## Step 1: Make sure that you have a valid go installation.
- Download GO
- Add <install_dir>/bin to your PATH
- Set GOROOT to <install_dir>
- Create a directory for your GO sources, and point GOPATH to it
- Make a subdirectory for this example, and copy these example files there

## Step 2: Download the cli library.
- $ go get github.com/urfave/cli

## Step 3: Build the example.
- chdir to the directory where the example is located
- $ go build

## Step 4: Run the example
- $ ./go_example --help
- $ ./go_example --count 10

## Step 5: Exercise
- Run the routine that prints results in a separate goroutine
- Add an extra parameter timeout, the timeout in nanoseconds.
- Create a second channel, named chQuit. The data type to be sent over it can be struct{}.
- After starting the Fibonacci generation, use the function time.Sleep to wait for the timeout.
- After this timsout, send something on chQuit.
- Modify the function fibonacci to stop when something is received on chQuit.

## Step 6: Verify the exercise.
- $ prezi_example --count 50 --timeout 50000
