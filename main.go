package main

import (
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli"
)

func fibonacci(n int, ch chan int) {
	x, y := 0, 1
	for i := 0; i < n; i++ {
		ch <- x
		x, y = y, x+y
	}
	close(ch)
}

func main() {
	var count int

	app := cli.NewApp()
	app.Name = "fibonacci"
	app.Usage = "Generate Fibonacci numbers."
	app.Flags = []cli.Flag{
		cli.IntFlag{
			Name:        "count",
			Value:       5,
			Usage:       "Number of Fibonacci numbers to generate.",
			Destination: &count,
		},
	}
	app.Action = func(ctx *cli.Context) error {
		ch := make(chan int, 10)
		go fibonacci(count, ch)
		for i := range ch {
			fmt.Println(i)
		}
		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
